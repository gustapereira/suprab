import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  ActivityIndicator,
  Image,
  Text,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Entypo from 'react-native-vector-icons/Entypo';
import {colors} from './../../../styles';
import Requisitions from './../../../Requisitions';
import styles from './styles';

import frenteCard from './../../../assets/images/SUPRAB-CarteiraAssociado-Frente.png';
import versoCard from './../../../assets/images/SUPRAB-CarteiraAssociado-Verso.png';

export default function Context({navigation}) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [filePath, setFilePath] = useState({});
  const [frente, setFrente] = useState(true);

  const Title = ({children}) => {
    return (
      <Text allowFontScaling={false} style={styles.title}>
        {children}
      </Text>
    );
  };

  const Description = ({children}) => {
    return (
      <Text allowFontScaling={false} style={styles.description}>
        {children}
      </Text>
    );
  };

  useEffect(() => {
    getData();
  }, []);

  async function getData() {
    debugger;
    const login = await AsyncStorage.getItem('loginSuprab');
    const parseLogin = JSON.parse(login);
    const response = await Requisitions.membro(parseLogin.cpf, parseLogin.cgp);
    if (response.ok) {
      const responseJson = await response.json();
      debugger;
      setData(responseJson);
      setTimeout(() => {
        setIsLoading(false);
      }, 2000);
    }
    const fotoPerfil = await AsyncStorage.getItem('fotoPerfil');
    const parseFotoPerfil = fotoPerfil ? JSON.parse(fotoPerfil) : null;
    setFilePath(parseFotoPerfil);
  }

  return (
    <>
      {isLoading ? (
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            marginTop: 50,
          }}>
          <ActivityIndicator color={colors.AMARELO_SUPRAB} size="large" />
        </View>
      ) : (
        <>
          {frente ? (
            <TouchableOpacity
              onPress={() => setFrente(false)}
              style={styles.container}>
              <View style={styles.card}>
                <ImageBackground
                  style={styles.containerDigital}
                  source={frenteCard}
                />
                <View style={styles.profile}>
                  {filePath !== null ? (
                    <Image
                      style={styles.profileImage}
                      source={{uri: filePath.uri}}
                    />
                  ) : (
                    <View style={{width: '100%', marginTop: 25, marginLeft: 5}}>
                      <Entypo name="user" color={colors.WHITE} size={100} />
                    </View>
                  )}
                </View>
                <View style={{marginLeft: 140}}>
                  <View style={styles.containerLine}>
                    <Title>CGP:</Title>
                    <Description>{` ${data.cgp}`}</Description>
                  </View>
                  <View style={styles.containerLine}>
                    <Title>NOME:</Title>
                    <Description>{` ${data.nome.toUpperCase()}`}</Description>
                  </View>
                  <View style={styles.containerLine}>
                    <Title>DATA DE NASCIMENTO:</Title>
                    <Description>{` ${data.dataNascimento.substring(8,10)}/${data.dataNascimento.substring(5,7)}/${data.dataNascimento.substring(0,4)}`}</Description>
                  </View>
                  <View style={styles.containerLine}>
                    <Title>CPF:</Title>
                    <Description>{` ${data.cpf.substring(0,3)}.${data.cpf.substring(3,6)}.${data.cpf.substring(6,9)}-${data.cpf.substring(9, 11)}`}</Description>
                  </View>
                </View>
                <View style={{marginTop: '8%', marginLeft: 10}}>
                  <View style={styles.containerLine}>
                    <View style={styles.containerLine}>
                      <Title>CIDADE:</Title>
                      <Description>{` ${data.endereco.cidade.toUpperCase()}`}</Description>
                    </View>
                    <View style={[styles.containerLine, {marginLeft: '10%'}]}>
                      <Title>UF:</Title>
                      <Description>{` ${data.endereco.uf.toUpperCase()}`}</Description>
                    </View>
                  </View>
                  <View style={styles.containerLine}>
                    <Title>CARGO:</Title>
                    <Description>{` ${data.cargo.toUpperCase()}`}</Description>
                  </View>
                  <View style={styles.containerLine}>
                    <Title>TÍTULO HONORÍFICO:</Title>
                    <Description>{` ${data.tituloHonorifico.toUpperCase()}`}</Description>
                  </View>
                  <View style={styles.containerLine}>
                    <View style={styles.containerLine}>
                      <Title>GRAU:</Title>
                      <Description>{` ${data.corposFilosoficos[0].grau.toUpperCase()}`}</Description>
                    </View>
                    <View style={[styles.containerLine, {marginLeft: '10%'}]}>
                      <Title>DATA DO GRAU:</Title>
                      <Description>{` ${data.corposFilosoficos[0].dataGrau.substring(8,10)}/${data.corposFilosoficos[0].dataGrau.substring(5,7)}/${data.corposFilosoficos[0].dataGrau.substring(0,4)}`}</Description>
                    </View>
                  </View>
                  <View style={styles.containerLine}>
                      <Title>CORPO FILOSÓFICO:</Title>
                    <Description>{` ${data.corposFilosoficos[0].corpo.toUpperCase()}`}</Description>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => setFrente(true)}
              style={styles.container}>
              <View style={styles.card}>
                <ImageBackground
                  style={styles.containerDigital}
                  source={versoCard}
                />
              </View>
            </TouchableOpacity>
          )}
        </>
      )}
    </>
  );
}
