import {StyleSheet, Dimensions} from 'react-native';
import {colors} from './../../../styles';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerDigital: {
    height: SCREEN_WIDTH * 0.9,
    width: SCREEN_HEIGHT * 0.9,
    resizeMode: 'cover',
    position: 'absolute',
  },
  containerDigitalVerso: {
    height: SCREEN_WIDTH * 0.9,
    width: SCREEN_HEIGHT * 0.9,
    transform: [{rotate: '90deg'}],
    resizeMode: 'cover',
    marginTop: 155,
    marginLeft: -135,
  },
  containerFoto: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  card_old: {
    height: SCREEN_WIDTH * 0.9,
    width: SCREEN_HEIGHT * 0.9,
    backgroundColor: colors.BLUE,
    // backgroundColor: '#006EC7',
    borderRadius: 20,
    transform: [{rotate: '90deg'}],
    padding: 16,
  },
  card: {
    height: SCREEN_WIDTH * 0.9,
    width: SCREEN_HEIGHT * 0.9,
    transform: [{rotate: '90deg'}],
    padding: 25,
  },
  title: {
    fontFamily: 'ocr-a-extended',
    color: '#000',
    fontWeight: 'bold',
    fontSize: 17,
    marginVertical: '0.5%',
  },
  description: {
    color: '#000',
    fontSize: 18,
    marginVertical: '0.5%',
    fontFamily: 'ocr-a-extended',
  },
  profile: {
    position: 'absolute',
    top: 20,
    left: 30,
    height: '52%',
    width: 130,
    backgroundColor: '#CCC',
    borderWidth: 1,
    borderColor: colors.AMARELO_SUPRAB,
  },
  profileImage: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'cover',
  },

  qrCode: {
    position: 'absolute',
    bottom: 16,
    right: 16,
    height: 150,
    width: 150,
    backgroundColor: 'white',
  },
  containerLine: {
    flexDirection: 'row',
  },
  stripe: {
    height: 25,
    marginTop: 10,
    width: 200,
    backgroundColor: '#FFF',
  },
});

