import {StyleSheet} from 'react-native';
import {colors} from '../../../styles';
export default StyleSheet.create({
  offset: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  tituloFoto: {
    color: '#FFF',
    fontSize: 14,
    marginLeft: 5,
  },
  container: {
    width: '100%',
    backgroundColor: '#FFF',
    borderRadius: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  containerButtonClose: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  title: {
    color: colors.BLACK,
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  description: {
    color: colors.BLACK,
    fontSize: 16,
    textAlign: 'center',
  },
});
