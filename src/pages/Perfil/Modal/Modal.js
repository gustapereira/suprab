/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Modal, View, Text, TouchableOpacity} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import styles from './Modal.styles';
import { ScrollView } from 'react-native-gesture-handler';
import {colors} from './../../../styles';

export default class AprendaFavoritos extends React.PureComponent {
  render() {
    const {visible, onClose, chooseFile, captureImage} = this.props;
    return (
      <Modal animationType="slide" transparent visible={visible}>
        <View style={styles.offset}>
          {/* <TouchableOpacity onPress={onClose} style={{flex: 1}} /> */}
          <View style={styles.container}>
            <View style={styles.header}>
              <TouchableOpacity
                onPress={onClose}
                style={styles.containerButtonClose}>
                <Text style={{fontSize: 16}}>X</Text>
              </TouchableOpacity>
            </View>
            {/* <Text style={styles.title}>{data.titulo}</Text> */}
            <View
              style={{flexDirection: 'column', justifyContent:'center', alignItems: 'center', width: '100%'}}
            >
              <TouchableOpacity
                activeOpacity={0.5}
                style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: colors.AZUL_SUPRAB,
                  width: '80%',
                  height: 40,
                  borderRadius: 10,
                }}
                onPress={chooseFile}>
                <Entypo name="images" color={colors.WHITE} size={16} />
                <Text style={styles.tituloFoto}>ALTERAR FOTO</Text>
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.5}
                style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: colors.AZUL_SUPRAB,
                  width: '80%',
                  height: 40,
                  borderRadius: 10,
                  marginBottom: 50
                }}
                onPress={captureImage}>
                <Entypo name="camera" color={colors.WHITE} size={16} />
                <Text style={styles.tituloFoto}>TIRAR UMA FOTO</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
